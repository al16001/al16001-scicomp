# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 23:58:31 2018

@author: Abbie Lund
    collocation(ode, #inputs x, t, par
                n, #number of collocation points
                T, #Time period (not used in this method)
                x0, #initial guess [x, x', ...]
                par, #parameters t input to ODE
                solver #method used to solve, default scipy fsolve
                )

Applies collocation to any supplied ODE using chebyshev points.
"""


import numpy as np
import pylab
from scipy.optimize import fsolve
from cheb import cheb
from week_5_collocation_tests import runtests_ode

def ode3(x, t, par): #duffing example ODE input
    """ Return dx/dt = f(x,t) at time t. """
    du0=x[1]
    du1=(-2*par[0])*x[1]-x[0]-x[0]**3+par[1]*np.sin(par[2]*t)
    return [du0, du1]
    """
    Compute the area and it's derivative as a function of independent variable x, return them as a vector.

    :param x: independent variable, the domain of the problem is x=0 to L
    :return: a 2-vec holding [A, dA/dx].
    """

def compute_x_prime(x, RHS, D, n, dim): #within BVP uses chebyshev matrix to approximate differentials
    x_prime=np.zeros((n, dim))
    for j in range(0, dim):
        x_prime[:, j]=np.dot(D[0:n,:],x[:, j])-RHS[0:n, j]
    return x_prime

def zero_problem(x, dim): #additional boundary conditions to be solved alongside the ODE
    boundary=np.zeros(dim)
    for i in range(0, dim): #first and last x, x', x''... the same
        boundary[i]=x[0, i]-x[-1, i]
    #first value and last value of every value in x should be the same
    return boundary

def BVP(x, ode, n, dim, par):  #make initial guess of the point positions, applying boundary conditions
     D, t=cheb(n)
     RHS=np.zeros((n+1, dim))
     x=x.reshape((n+1, dim), order="F")
     #this part will iterate and change the input into the ode with each iteration
    #loop required to take input from tests which only returns values for a single point in time seperately.
     for i in range(0, n):
         RHS[i, :]=ode(x[i, :], t[i], par)
     xpr=compute_x_prime(x, RHS, D, n, dim)
     boundary=zero_problem(x, dim)
     assert xpr.shape==(n, dim)
     BVP=np.vstack((xpr, boundary))
     assert BVP.shape==x.shape
     BVP=BVP.reshape((dim*(n+1),), order="F")
     return BVP




def collocation(ode, n, T, x0, par, solver):
    D, t=cheb(n) #not required as this is found in every loop of BVP, but assertion here confirms shape on first loop
    N=np.size(t)
    assert D.shape==(N, N)
    assert t.shape==(N,)
    if x0.shape==(N,):
        dim=1
    else:
        (N, dim)=x0.shape
        RHS=np.zeros((n, dim)) #declare the variable, this will contain the results of all linear ODES which should approach zeros.
    #iterate to the point where x, x' stop changing and boundaries are the same
    x0=solver(BVP, x0, args=(ode, n, dim, par))
    return x0 #vector of all x, x', x''...

#####UNCOMMENT FOR EXAMPLE AND TESTS WITH OUTPUTS FROM COLLOCATION#######
#collocation(ode3, n, np.zeros((n+1, 2)), par)
#runtests_ode(collocation)
