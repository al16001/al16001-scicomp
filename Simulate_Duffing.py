# -*- coding: utf-8 -*-
"""
Created on Wed Oct 24 11:19:20 2018

@author: al16001
"""

import numpy as np 
from scipy.integrate import odeint 
import pylab

# parameters
E=0.05
T=0.2
w=1.2


# A suitable grid of time points 
t = np.linspace(0, 100, 2000)

def deriv(u, t, E, T, w): 
    """ Return dx/dt = f(x,t) at time t. """ 
    du0=u[1]
    du1=-(2*E)*u[1]-u[0]-u[0]**3+T*np.sin(w*t)
    return du0, du1

#inital conditions u=0 at t=0
u0=(0,0)    

# Integrate the differential equations 
u, udot = odeint(deriv, u0, t, args=(E, T, w)).T
pylab.plot(t, udot)
pylab.plot(t, u)
pylab.show

def boundaryConditions():
    print()
def numericalintegrator():
    print()
def rootfinder():
    print()