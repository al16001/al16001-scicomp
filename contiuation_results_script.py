# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 11:05:49 2018

@author: hp
"""
from scipy.optimize import fsolve
from con_gen_without_pseudo import *

solver=fsolve

ode=damper
steps=50
#n=30 #points in the collocation
kfinal=20
n=0
T=2
x0=[0, 0]
#x0=np.zeros((n+1, 2))

par=[0.05, 0.1]
#method=collocation
#method(ode, n, x0, par)
method=shooting

ran1, xmax1=continuation(ode, steps, n, T, x0, kfinal, par, method, solver)



ode=damper
steps=50
#n=30 #points in the collocation
kfinal=20
n=30
T=2
x0=np.zeros((n+1, 2))

par=[0.05, 0.1]
#method=collocation
#method(ode, n, x0, par)
method=collocation

ran2, xmax2=continuation(ode, steps, n, T, x0, kfinal, par, method, solver)

#Plot
fig = plt.figure()
plt.plot(ran1, xmax1, '.-', ran2, xmax2, '.-')
plt.legend(['Shooting', 'Collocation'])
fig.suptitle('Mass Spring Damper Continuation')
plt.xlabel('k')
plt.ylabel('||x||')
fig.savefig('test.jpg')


ode=ode3
steps=40
#n=30 #points in the collocation
kfinal=20
n=0
T=2
x0=[0, 0]

par=[0.05, 0.2, 0.5, 0.1]
#method=collocation
#method(ode, n, x0, par)
method=shooting

ran3, xmax3=continuation(ode, steps, n, T, x0, kfinal, par, method, solver)
#Plot

ode=ode3
steps=40
#n=30 #points in the collocation
kfinal=20
n=30
T=2
x0=np.zeros((n+1, 2))

par=[0.05, 0.2, 0.5, 0.1]
#method=collocation
#method(ode, n, x0, par)
method=collocation

ran4, xmax4=continuation(ode, steps, n, T, x0, kfinal, par, method, solver)
#Plot
fig2 = plt.figure()
plt.plot(ran3, xmax3, '.-', ran4, xmax4, '.-')
plt.legend(['Shooting', 'Collocation'])
fig2.suptitle('Duffing Continuation')
plt.xlabel('k')
plt.ylabel('||x||')
fig2.savefig('test2.jpg')