# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 23:49:10 2018

@author: hp
"""
from coll_gen import *
import numpy as np
import pylab
from scipy.optimize import fsolve
from week_4 import *
def duff(x, t, par): #example ODE input
    """ Return dx/dt = f(x,t) at time t. """ 
    du0=x[1]
    du1=(-2*par[0])*x[1]-(par[3]*x[0])-(par[1]*x[0]**3)+par[2]*np.sin(np.pi*t)
    return [du0, du1]

def damper(x, t, par): #example ODE input
    du0=x[1]
    du1=(-2*par[0])*x[1]-(par[1]*x[0])+np.sin(np.pi*t)
    return [du0, du1]

def BVP2(y, ode, n, dim, par):
     x=y[0:-1]
     x=np.reshape(x, (n+1, 2)) #this only works for 2D
     D, t=cheb(n)
     RHS=np.zeros((n+1, dim))
     #x=x.reshape((n+1, dim), order="F")
     for i in range(0, n):
         RHS[i, :]=ode(x[i, :], t[i], par)
     xpr=compute_x_prime(x, RHS, D, n, dim)
     boundary=zero_problem(x, dim)
     assert xpr.shape==(n, dim)
     BVP2=np.vstack((xpr, boundary))
     assert BVP2.shape==x.shape
     BVP2=BVP2.reshape((dim*(n+1),), order="F")
     return BVP2

def augmented(y, yhat, secant, par, ode, n, method, Dim):
    T=1;
    x0=y[0:-1]
    x0=np.reshape(x0, (n+1, Dim))
    f=method(ode, n, T, x0, par, solver)
    # The original equation
    pseudo=np.dot(secant.T,(y - yhat));       #The pseudo-arclength condition
    z=np.append(f, pseudo)
    return z

def continuation(ode, steps, n, x0, ran, par, method, solver):
    if n==0: #shooting
        (Dim,)=np.shape(x0)
    if n>0: #collocation
        (unrequired, Dim)=np.shape(x0)
    T=0
    savex0=[]
    xx=np.zeros((steps+2, Dim*(n+1)))

    par[-1]=1
    pp=np.zeros((steps+2, 1))

    x0=collocation(ode, n, T, x0, par, solver)
    savex0=np.reshape(x0, (1,Dim*(n+1)))
    xx[0, :]=savex0[:]
    pp[0]=par[-1]

    par[-1]=1.1

    x0=np.reshape(x0, (n+1, Dim))
    x1=collocation(ode, n, T, x0, par, solver)
    savex1=np.reshape(x1, (1, Dim*(n+1)))
    xx[1, :]=savex1[:]
    pp[1]=par[-1]

    y0=np.append(savex0, pp[0])
    y1=np.append(savex1, pp[1])
    
    for i in range(0, steps):
        # Predict
        secant = np.array(y1 - y0);
        y2hat = (y1 + secant);
        # Correct
        y2 = solver(augmented, y2hat, args=(y2hat, secant, par, ode, n, method, Dim));
        # Store
        xx[i + 2] = y2[0:-1];
        pp[i + 2] = y2[-1];
        par[-1]=y2[-1]
        # Rotate the variables for the next iteration
        y0 = y1;
        y1 = y2;

    xx=xx[:, 0:n+1]
    xmax=np.zeros((steps+2, 1))
    for i in range(0,steps+2):
        data=abs(xx[i, :])
        xmax[i]=max(data)
    print(xmax)
    print(pp)
    return xmax, pp

steps=20
n=15 #points in the collocation
ran=np.linspace(0.1, 14, steps)
x0=np.zeros((n+1, 2))
ode=damper
solver=fsolve
method=collocation
#par=[0.05, 1, 0.5, k0]
par=[0.05, 0.1]

xmax, pp=continuation(ode, steps, n, x0, ran, par, method, solver)
#Plot

#Plot
fig2 = plt.figure()
plt.plot(pp, xmax, '.-')
fig2.suptitle('Attempt to use pseudo arclength condition on Damper equation')
plt.xlabel('k')
plt.ylabel('x')
fig2.savefig('test3.jpg')
