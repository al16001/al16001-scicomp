# -*- coding: utf-8 -*-
"""
continuation(ode,
             steps,
             n,
             T,
             x0,
             kfinal,
             par,
             method,
             solver)

ode: takes input x, t, par, final par should be the one to vary
par: in the form [a, b, c, k] where k is the parameter to vary
    at the initial choice. (e.g. start at k=0.1 and increment to kfinal)
kfinal: final maximum value of k to be investigated
steps: number of continuation steps incremented from initial choice of k (from par) and kfinal.
solver: currently works for fsolve
n: if using collocation, number of points after the starting point
    if using shooting, n=0
T: if using shooting, time period of ODE,
    if using collocation, set as zero
x0: initial value, column depending on dimension: [x, x', x'']
    x is a column of values of length n+1 for collocation
method: shooting or collocation

"""
from coll_gen import *
from week_4 import *
import numpy as np
import pylab
from matplotlib import pyplot as plt

from scipy.optimize import fsolve

def ode3(x, t, par):
    """ Return dx/dt = f(x,t) at time t. """
    du0=x[1]
    du1=(-2*par[0])*x[1]-(par[3]*x[0])-(par[1]*x[0]**3)+par[2]*np.sin(np.pi*t)
    return [du0, du1]

def damper(x, t, par):
    du0=x[1]
    du1=(-2*par[0])*x[1]-(par[1]*x[0])+np.sin(np.pi*t)
    return [du0, du1]


def continuation(ode, steps, n, T, x0, kfinal, par, method, solver=fsolve):
    if n==0: #shooting
        (Dim,)=np.shape(x0)
    if n>0: #collocation
        (unrequired, Dim)=np.shape(x0)
    ran=np.linspace(par[-1], kfinal, steps)
    savex0=[]
    xx=np.zeros((steps, Dim*(n+1)))
    i=0
    for k in ran:
        x0=np.array(x0)
        x0=x0.reshape(n+1, 2) #this only works for 2D
        par[-1]=k
        x0=method(ode, n, T, x0, par, solver)
        if method==shooting:
            t = linspace(0, T, 101)
            x = odeint(ode, x0, t, args=(par,))
            x=x[:, 0:n+1]
            x=abs(x)
            x0=[max(x), max(x)]
        savex0=np.reshape(x0, (1, Dim*(n+1)))
        xx[i, :]=savex0[:]
        i=i+1

    xx=xx[:, 0:n+1]
    xmax=np.zeros((steps, 1))
    for i in range(0,steps):
        data=abs(xx[i, :])
        xmax[i]=max(data)
    return ran, xmax
