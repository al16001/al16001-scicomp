from week_4 import *
from numpy import *
# Import the ODE integrator
from scipy.integrate import odeint
# Get access to the root-finder
from scipy.optimize import fsolve
# Plotting
from matplotlib.pyplot import *


#Test the code on the Duffing equation
xi = 0.05
Gamma = 0.2
omega = 1.2
pars = [xi, Gamma, omega]
x0 = [0, 1]
# Get the corrected starting point
x0 = shooting(duffing,n, 2*pi/omega,x0, pars, solver)
# Integrate from the corrected starting point to see the periodic orbit
t = linspace(0, 2*pi/omega, 101)
x = odeint(duffing, x0, t, args=(pars,))
plot(t, x)
show()

# Test the code on the mass-spring-damper equation - note how the only
# difference is to change the name in the shooting function call! This is the
# sign of properly compartmentalised code.
xi = 0.05
Gamma = 0.2
omega = 1.2
pars = [xi, Gamma, omega]
x0 = np.array((1, 1.0))
n=2
# Get the corrected starting point
x0 = shooting(massspringdamper, n, 2*pi/omega, x0, pars, solver)
# Integrate from the corrected starting point to see the periodic orbit
t = linspace(0, 2*pi/omega, 101)
x = odeint(massspringdamper, x0, t, args=(pars,))
plot(t, x)
show()