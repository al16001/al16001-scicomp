# This code is designed to be general and modular. Notice how the zero problem
# does not depend on duffing directly but takes it as a parameter.

# Import everything from numpy
from numpy import *
# Import the ODE integrator
from scipy.integrate import odeint
# Get access to the root-finder
from scipy.optimize import fsolve
# Plotting
from matplotlib.pyplot import *


def zeroproblem(x, ode, T, pars):
    """
    zeroproblem(x, ode, T, pars)

    Define a zero problem f(x) = x(T) - x(0) for numerical shooting.
    """
    return x - odeint(ode, x, [0, T], args=(pars,))[-1, :]


def shooting(ode,n, T, x0, pars, solver):
    """
    shooting(ode, x0, T, pars)

    Solve a periodic boundary value problem for the specified ODE, given a
    starting guess, the period and parameters.
    """
    xnew, info, ier, mesg = solver(zeroproblem, x0, args=(ode, T, pars), full_output=True)
    if ier == 1:
        return xnew
    else:
        return nan  # return Not a Number if the method fails to converge


def duffing(x, t, pars):
    """
    duffing(x, t, pars)

    Return the right-hand side of the Duffing differential equation in
    first-order form. The equation is given by

        x'' + 2*xi*x' + x + x^3 = Gamma*sin(omega*t)

    Parameters to the function
        x
            State variables [x1, x2]
        t
            Time
        pars
            System parameters [xi, Gamma, omega]
    """
    return [x[1], pars[1]*sin(pars[2]*t) - 2*pars[0]*x[1] - x[0] - x[0]**3]


def massspringdamper(x, t, pars):
    """
    massspringdamper(x, t, pars)

    Return the right-hand side of the mass-spring-damper differential equation
    in first-order form. The equation is given by

        x'' + 2*xi*x' + x = Gamma*sin(omega*t)

    Parameters to the function
        x
            State variables [x1, x2]
        t
            Time
        pars
            System parameters [xi, Gamma, omega]
    """
    return [x[1], pars[1]*sin(pars[2]*t) - 2*pars[0]*x[1] - x[0]]



