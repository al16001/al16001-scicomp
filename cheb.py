import numpy as np
import matplotlib.pyplot as plt
from week_5_chebtests import runtests_cheb
def cheb(N):
#CHEB  compute D = differentiation matrix, x = Chebyshev grid
    if N==0 :
        D=0; x=1;
        return
    range1=np.arange(0,N+1)
    x = np.cos(np.pi*range1/N).T;

    c=np.vstack((2, np.ones((N-1, 1)), 2))*(-1)**(range1.T)
    for i, value in enumerate(c, 1):
        value=value*(-1**(i-1))
        np.insert(c, i, value)
    X = np.tile(x,(N+1, 1));
    dX = X-X.T;
    D  = (c*((1/c).T))/(dX+(np.eye(N+1)))       # off-diagonal entries
    sumDt=[sum(x) for x in zip(*D.T)]
    D  = D - np.diag(sumDt);                #  diagonal entries
    return D, x
#D, x=cheb(10)
#print(np.size(D))
#print(np.size(x))
#actualx= np.linspace(-np.pi, np.pi, 100)
#actualu=np.sin(actualx)
#u=np.sin(x)
#plt.plot(x, u, 'bo', actualx, actualu, 'r')

#runtests_cheb(cheb)
    