# -*- coding: utf-8 -*-

import numpy as np
import pylab
from scipy.optimize import fsolve
""" @author: hp
"""

#This will work for vector valued functions, it just assumes the vector is
# a column vector. (You'll also need to change the starting guess for the
# initial point.)

# Define the normal form for a saddle-node bifurcation
def function(x, p):
    #x=y[:,0]; p=y[:,1];
    f=x**3-x-p
    return f

# Find a starting point for p = 1 from starting guess of 1
p0 = 2;
x0 = fsolve(function, 1, p0);

# Find a second starting point for p = 0.9 from starting guess of x0
p1 = 1.9;
x1 = fsolve(function, x0, p1);

# Define the augmented system (with the pseudo-arclength condition)
#   y = [x, p];
#   yhat = predicted point
#   secant = secant of the last two points

def augmented(y, yhat, secant):
    y=np.reshape(y, (2,1))
    yhat=np.reshape(yhat, (2, 1))
    #all values but the last one are x
    #last value is p
    f=function(y[0:-1,0], y[-1, 0])
    # The original equation
    pseudo=np.dot(secant.T,(y - yhat));       #The pseudo-arclength condition
    z=[f, pseudo]
    z=np.reshape(z, (2,))
    return z



#Predict and correct, repeatedly
niter = 40;    
                 #Do 30 steps
(x0length,)=x0.shape
print(x0)
print(x1)
xx = np.vstack((x0, x1, np.zeros((niter, x0length))));
#Store the results (state)
pp = np.vstack((p0, p1, np.zeros((niter, 1))));  
y0=np.vstack((x0, p0))   
y1=np.vstack((x1, p1))      
#Store the results (parameter)

for i in range(0,niter):
    # Predict
    secant = np.array(y1 - y0);
    y2hat = (y1 + secant);
    # Correct
    y2 = fsolve(augmented, y2hat, args=(y2hat, secant));
    # Store
    xx[i + 2] = y2[0];
    pp[i + 2] = y2[1];
    # Rotate the variables for the next iteration
    y0 = y1;
    y1 = np.reshape(y2,(2, 1));

#Plot
fig2 = plt.figure()
plt.plot(pp, xx, '.-')
fig2.suptitle('Cubic Equation')
plt.xlabel('k')
plt.ylabel('x')
fig2.savefig('test3.jpg')