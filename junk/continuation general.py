
import numpy as np
import pylab
from scipy.optimize import fsolve
from cheb import cheb
from week_5_collocation_tests import runtests_ode

def function(x, p):
    #x=y[:,0]; p=y[:,1];
    f=x**3-x-p
    return f

def ode2(x, t, par):
    """
    ode2(x, t, par)

    Return the right-hand-side of the ODE

        x'' + par[0]*x' + par[1]*x = sin(pi*t)
        
limitation is i had to change the tests for np.sinand np.pi, can i just import numpy without use of prefix?
    """
    return [x[1], np.sin(np.pi*t) - par[0]*x[1] - par[1]*x[0]]

def ode1(x, t, par):
    """
    ode1(x, t, par)

    Return the right-hand-side of the ODE

        x' = sin(pi*t) - x
    """
    return np.sin(np.pi*t) - x
 
def ode3(x, t, pars): 
    """ Return dx/dt = f(x,t) at time t. """ 
    du0=x[1]
    du1=(-2*par[0])*x[1]-(par[1]*x[0])-(par[2]*x[0]**3)+par[3]*np.sin(np.pi*t)
    return [du0, du1]
    """
    Compute the area and it's derivative as a function of independent variable x, return them as a vector.
    
    :param x: independent variable, the domain of the problem is x=0 to L
    :return: a 2-vec holding [A, dA/dx].
    """

def compute_x_prime(x, RHS, D, n, dim):
    x_prime=np.zeros((n, dim))
    for j in range(0, dim):
        x_prime[:, j]=np.dot(D[0:n,:],x[:, j])-RHS[0:n, j]
    return x_prime
    
def zero_problem(x, dim):
    boundary=np.zeros(dim)
    for i in range(0, dim):
        boundary[i]=x[0, i]-x[-1, i]
    #first value and last value of every value in x should be the same 
    return boundary

def BVP1(x,ode, n, dim, par):
     D, t=cheb(n)
     RHS=np.zeros((n+1, dim))
     x=x.reshape((n+1, dim), order="F")
     for i in range(0, n):
         RHS[i, :]=ode(x[i, :], t[i], par)
     xpr=compute_x_prime(x, RHS, D, n, dim)
     boundary=zero_problem(x, dim)
     assert xpr.shape==(n, dim)
     BVP=np.vstack((xpr, boundary))
     assert BVP.shape==x.shape
     BVP=BVP.reshape((dim*(n+1),), order="F")
     return BVP
 
def BVP(y, yhat, secant, ode, n, dim, par):
     x=y[0:-1]
     par[1]=y[-1]
     D, t=cheb(n)
     RHS=np.zeros((n+1, dim))
     x=x.reshape((n+1, dim), order="F")
     for i in range(0, n):
         RHS[i, :]=ode(x[i, :], t[i], par)
     xpr=compute_x_prime(x, RHS, D, n, dim)
     boundary=zero_problem(x, dim)
     assert xpr.shape==(n, dim)
     BVP=np.vstack((xpr, boundary))
     assert BVP.shape==x.shape
     BVP=BVP.reshape((dim*(n+1),), order="F")
     pseudo=augmented(y, yhat, secant)
     BVP=np.append(BVP, pseudo)
     return BVP


def augmented(y, yhat, secant):
    pseudo=np.dot(secant.T,(y - yhat));       #The pseudo-arclength condition
    return pseudo



#Predict and correct, repeatedly
niter = 3;    
                 #Do 30 steps
# Find a starting point for p = 1 from starting guess of 1
p0 = 0.1;
p1 = 0.5;
n=3
ode=ode3
dim=2

par=[0.05, p0, 1, 0.5]
x0=np.zeros((1,2*(n+1)))
x0=BVP1(x0, ode, n, dim, par)
print(x0)
par=[0.05, p0, 1, 0.5]
x1=BVP1(x0, ode, n, dim, par)
print(np.vstack((x0, x1)))
xx = np.vstack((x0, x1, np.zeros((niter, 2*(n+1)))));
print(xx)
#Store the results (state)
pp = np.vstack((p0, p1, np.zeros((niter, 1))));  
y0=np.append(x0, p0)   
y1=np.append(x1, p1)      

for i in range(0,niter):
    # Predict
    secant = np.array(y1 - y0);
    y2hat = (y1 + secant);
    # Correct
    y2 = fsolve(BVP, y2hat, args=(y2hat, secant, ode, n, dim, par));
    print(y2)
    # Store
    xx[i + 2, :] = y2[0:-1];
    pp[i + 2] = y2[-1];
    # Rotate the variables for the next iteration
    y0 = y1;
    #y1 = np.reshape(y2,(2, 1));
    y1=y2;
xx=xx[:, 0:n+1]
xmax=np.zeros((niter+2, 1))
for i in range(0,niter+2):
    data=xx[i, :]
    xmax=max(data)
    #x0=fsolve(BVP, x0, args=(ode, n, dim))
#Plot
#pylab.plot(pp, xmax, '.-');