# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 19:32:28 2018

@author: hp
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 11:51:42 2018

@author: hp
"""
from cheb import cheb
import numpy as np 
import pylab
from scipy.optimize import fsolve

def compute_yprime(y, x, D, N, E, T, w):
    y1=y[0:N-1]
    y2=y[N:2*N-1]
    y1dot=D[1:-1,:].dot(y1) #10, 1
    return y1dot-T*np.sin(w*x[0:N-1])
    #should x also skip the first entry which is fixed?
    #add fixed initial conditions equation

def zero_problem(y, N):
    y1=y[0:N-1]
    y2=y[N:2*N-1]
    return y1[0]-y1[-1], y2[0]-y2[-1]

def fun(y,x, D, N, E, T, w):
    F=np.zeros((3*N))
    F[0:N-1], F[N:2*N-1]=zero_problem(y, N)
    F[2*N:3*N-1]=compute_yprime(y, x, D, N, E, T, w)
    return F
    
def collocation(n):
    # parameters
    E=0.05
    T=0.2
    w=1.2
    D, x=cheb(n)  
    N=np.size(x)

    #make initial guess of the point positions, first and last points are same
    #make initial guess of point gradients, first and last gradients are same
    y0=np.zeros((N,3), dtype=np.int8)
    #y0=np.vstack(np.zeros(N), np.zeros(N))
    y=fsolve(fun, y0, args=(x, D, N, E, T, w))
    

    pylab.plot(x, y[0:N-1])
    pylab.plot(x, y[N:2*N-1])
    pylab.show
    
collocation(10)