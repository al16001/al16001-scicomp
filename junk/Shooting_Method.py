# -*- coding: utf-8 -*-
"""
Created on Sun Oct 28 14:09:16 2018

@author: hp
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib

from scipy.optimize import least_squares
from scipy.integrate import ode

def deriv(u, t, E, T, w): 
    """ Return dx/dt = f(x,t) at time t. """ 
    du0=u[1]
    du1=-(2*E)*u[1]-u[0]-u[0]**3+T*np.sin(w*t)
    return [du0, du1]
    """
    Compute the area and it's derivative as a function of independent variable x, return them as a vector.
    
    :param x: independent variable, the domain of the problem is x=0 to L
    :return: a 2-vec holding [A, dA/dx].
    """


def compute_zprime(x, z, areafunction):
    """
    Compute the value of the vector z's derivative at a point given the value of the vector function z and the
    independent variable x. The form of this calculation is specified by the vector ODE. Return a vector for the
    derivative.
    
    :param x: indpendent variable, the domain of the problem is x=0 to L
    :param z: 2-vec of the variable representing system of equations, z = [y, y']
    :param areafunction: a function that takes x and gives back a 2-vec [A(x), dA(x)/dx]
    :return: 2-vec of values for z' = f(x, z)
    """
    zprime_0 = z[1]
    area, areaprime = areafunction(x)
    zprime_1 = - areaprime/area * z[1]
    return [zprime_0, zprime_1]

def integrate_over_domain(z_at_0, integrator, areafunction, length=10, step=0.1):
    """
    Call runge-kutta repeatedly to integrate the vector function z over the full domain (specified by length). Return
    a list of 2-vecs which are the value of the vector z at every point in the domain discretized by 'step'. Note that
    runge-kutta object calls x as "t" and z as "y".
    
    :param z_at_0: the value of the vector z=[y, y'] at the left boundary point. should be list or array.
    :param integrator: the runge-kutta numerical integrator object
    :param areafunction: a function that takes x and gives back a 2-vec [A(x), dA(x)/dx]
    :param length: the length of the domain to integrate on
    :param step: the step size with which to discretize the domain
    :param silent: bool indicating whether to print the progress of the integrator
    :return: array of 2-vecs - the value of vector z obtained by integration for each point in the discretized domain.
    """
    initial_conditions = z_at_0
    integrator.set_initial_value(initial_conditions, t=0)  # Set the initial values of z and x
    integrator.set_f_params(areafunction)
    dt = step

    xs, zs = [], []
    while integrator.successful() and integrator.t <= length:
        integrator.integrate(integrator.t + dt)
        xs.append(integrator.t)
        zs.append([integrator.y[0], integrator.y[1]])
    return xs, zs


def solve_bvp_tj(integrator, y_at_0, y_at_length, areafunction, length=10, step=0.1):
    """
    Numerically find the value for y'(0) that gives us a propagated (integrated) solution matching most closely with
    with other known boundary condition y(L) which is proportional junction temperature.

    :param y_at_0: the known boundary condition y(0) for the left point.
    :param y_at_length: the known boundary condition y(L) for the right point
    :param areafunction: a function that takes x and gives back a 2-vec [A(x), dA(x)/dx]
    :param length: the length of the domain to integrate on
    :param step: the step size with which to discretize the domain
    :param silent: bool indicating whether to print the progress of the integrator
    :return: the optimized estimate of y' at the left boundary point giving the most accurate integrated solution.
    """
    z_at_0 = [y_at_0, 0.5]  # Make an initial guess for yprime at x=0

    def residuals(yprime_at_0, y_at_0, y_at_length):
        # Use RK to compute [y, y'] over the domain given the values for y, y' at the boundary
        z_at_0 = [y_at_0, yprime_at_0]
        xs, zs = integrate_over_domain(z_at_0, integrator, areafunction)
        y_at_length_integrated = np.array(zs)[-1, 0]

        # Return the difference between y(L) found by numerical integrator and the true value
        return y_at_length - y_at_length_integrated

    yprime_at_0_guess = 0.2
    lsq = least_squares(residuals, yprime_at_0_guess, args=(y_at_0, y_at_length), loss="soft_l1")
    yprime_at_0_estimate = lsq.x[0]
    return yprime_at_0_estimate

areaf = deriv(u, t, 0.05, 0.2, 1.2)

integrator = ode(compute_zprime).set_integrator("dopri5")
y_at_length = 300
fig, ax = plt.subplots()
tenvs = np.arange(100, 260, 10)

pjs = []
for y_at_0 in tenvs:
    integrator.set_initial_value([y_at_0, 0.5], t=0)  # Set the initial values of z and x
    integrator.set_f_params(areaf)
    yprime_at_0_estimate = solve_bvp_tj(integrator, y_at_0, y_at_length, areaf)
    xs, zs = integrate_over_domain([y_at_0, yprime_at_0_estimate], integrator, areaf)
    pjs.append(np.array(zs)[-1,1])
ax.plot(tenvs, pjs, label=nm)

ax.legend(loc="best")
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
ax.text(0.5, 0.95, "$T_j$ = %.0f" % (y_at_length,), transform=ax.transAxes, fontsize=14,
        verticalalignment='top', bbox=props)
plt.tight_layout()