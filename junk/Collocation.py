import numpy as np
import pylab
from scipy.optimize import fsolve
from cheb import cheb
from week_5_collocation_tests import runtests_ode

def compute_yprime(y, x, D, N, E, T, w):
    y1, y2=np.split(y, 2)
    y1dot=np.dot(D[1:N,:],y1) #10, 1
    y2dot=np.dot(D[1:N,:],y2) #10, 1
    return [y1dot-y2[1:N], y2dot-(2*E)*y2[1:N]+y1[1:N]+y1[1:N]**3-T*np.sin(w*x[1:N])]


def zero_problem(y, N):
    y1, y2=np.split(y,2)
    assert y1.shape==(N,)
    assert y2.shape==(N,)
    print(y1[0])
    print(y1[-1])
    return [y1[0]-y1[-1], y2[0]-y2[-1]]

def fun(y,x, D, N, E, T, w):
    boundary1, boundary2=zero_problem(y, N)
    ODE1, ODE2=compute_yprime(y, x, D, N, E, T, w)
    ODE1=ODE1.reshape((N-1, 1))
    ODE2=ODE2.reshape((N-1, 1))
    c=np.vstack((boundary1, ODE1))
    d=np.vstack((boundary2, ODE2))
    newy=np.vstack((c, d))
    newy=newy.reshape(2*N,)
    return newy

def collocation(ode, n, x0, pars):
    # parameters
    E=0.05
    T=0.2
    w=1.2
    D, x=cheb(n)
    N=np.size(x)
    assert D.shape==(N, N)
    assert x.shape==(N,)
    #make initial guess of the point positions, first and last points are same
    #make initial guess of point gradients, first and last gradients are same
    y0=np.zeros((N, 2))
    assert y0.shape==(N, 2)
    y0=y0.flatten(order='C')
    y0=fsolve(fun, y0, args=(x, D, N, E, T, w))
    y1=y0[0:N]
    y1dot=y0[N:2*N]
    pylab.plot(x, y1dot)
    pylab.plot(x, y1)
    pylab.show

ode=1;
n=10;
x0=0;
pars=1;
runtests_ode(collocation)