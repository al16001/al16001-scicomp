# This code is designed to be general and modular. Notice how the zero problem
# does not depend on duffing directly but takes it as a parameter.

# Import everything from numpy
from numpy import *
# Import the ODE integrator
from scipy.integrate import odeint
# Get access to the root-finder
from scipy.optimize import fsolve
# Plotting
from matplotlib.pyplot import *


def zeroproblem(x, t, M, ode, T, pars):
    """
    zeroproblem(x, ode, T, pars)

    Define a zero problem f(x) = x'-sin(wt)-x for collocation.
    """
    return M*x-math.sin(omega*t)-x

def collocation(ode, x0, t, M, T, pars):
    """x0 is now a vector of x values, t is a vector of time increments"""
        xnew, info, ier, mesg = fsolve(zeroproblem, x0, t, args=(ode, T, pars), full_output=True)
        if ier == 1:
            return xnew
        else:
            return nan  # return Not a Number if the method fails to converge

#modified for this weeks now method
def shooting(ode, x0, T, pars):
    """
    shooting(ode, x0, T, pars)

    Solve a periodic boundary value problem for the specified ODE, given a
    starting guess, the period and parameters.
    """
    xnew, info, ier, mesg = fsolve(zeroproblem, x0, args=(ode, T, pars), full_output=True)
    if ier == 1:
        return xnew
    else:
        return nan  # return Not a Number if the method fails to converge

#Dont require
def duffing(x, t, pars):
    """
    duffing(x, t, pars)

    Return the right-hand side of the Duffing differential equation in
    first-order form. The equation is given by

        x'' + 2*xi*x' + x + x^3 = Gamma*sin(omega*t)

    Parameters to the function
        x
            State variables [x1, x2]
        t
            Time
        pars
            System parameters [xi, Gamma, omega]
    """
    return [x[1], pars[1]*sin(pars[2]*t) - 2*pars[0]*x[1] - x[0] - x[0]**3]

def week_5_equation(x, t, pars)

# Test the code on the Duffing equation
xi = 0.05
Gamma = 0.2
omega = 1.2
pars = [xi, Gamma, omega]
x0 = [0.0, 1.0]
# Get the corrected starting point
x0 = shooting(duffing, x0, 2*pi/omega, pars)
# Integrate from the corrected starting point to see the periodic orbit
t = linspace(0, 2*pi/omega, 101)
x = odeint(duffing, x0, t, args=(pars,))
plot(t, x)
show()


def massspringdamper(x, t, pars):
    """
    massspringdamper(x, t, pars)

    Return the right-hand side of the mass-spring-damper differential equation
    in first-order form. The equation is given by

        x'' + 2*xi*x' + x = Gamma*sin(omega*t)

    Parameters to the function
        x
            State variables [x1, x2]
        t
            Time
        pars
            System parameters [xi, Gamma, omega]
    """
    return [x[1], pars[1]*sin(pars[2]*t) - 2*pars[0]*x[1] - x[0]]


# Test the code on the mass-spring-damper equation - note how the only
# difference is to change the name in the shooting function call! This is the
# sign of properly compartmentalised code.
xi = 0.05
Gamma = 0.2
omega = 1.2
pars = [xi, Gamma, omega]
x0 = [0.0, 1.0]
# Get the corrected starting point
x0 = shooting(massspringdamper, x0, 2*pi/omega, pars)
# Integrate from the corrected starting point to see the periodic orbit
t = linspace(0, 2*pi/omega, 101)
x = odeint(massspringdamper, x0, t, args=(pars,))
plot(t, x)
show()
