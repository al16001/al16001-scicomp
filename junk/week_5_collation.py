<<<<<<< HEAD
from scipy.integrate import odeint
import pylab
from scipy.optimize import fsolve

# parameters
E=0.05
T=0.2
w=1.2


# A suitable grid of time points
t = np.linspace(0, 100, 2000)


def duffing(E, T, w):
    u0, u0dot, u0dotdot=collocation(10)
    u0dotdot=-(2*E)*u0dot-u0-u0**3+T*np.sin(w*t)

def compute_yprime(x, y, D):
    y1, y2=y[:,1], y[:,2]
    y1dot=np.dot(D[2:N,:],y1)
    y2dot=np.dot(D[2:N,:],y2)
    return [y1dot-y2[2:N], y2dot-(2*E)*y2[2:n]+y1[2:N]+y1[2:N]**3-T*np.sin(w*x)]
    #should x also skip the first entry which is fixed?
    #add fixed initial conditions equation




def collocation(N):
    #inital conditions u=0 at t=0
    D, x=cheb(N)

=======
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 11:51:42 2018

@author: hp
"""
from cheb import cheb
import numpy as np 
import pylab
from scipy.optimize import fsolve

def compute_yprime(y, x, D, N, E, T, w):
    y1, y2=np.split(y, 2)
    y1dot=np.dot(D[1:N,:],y1) #10, 1
    y2dot=np.dot(D[1:N,:],y2) #10, 1
    return [y1dot-y2[1:N], y2dot-(2*E)*y2[1:N]+y1[1:N]+y1[1:N]**3-T*np.sin(w*x[1:N])]
    #should x also skip the first entry which is fixed?
    #add fixed initial conditions equation

def zero_problem(y, N):
    y1, y2=np.split(y,2)
    assert y1.shape==(N,)
    assert y2.shape==(N,)
    print(y1[0])
    print(y1[-1])
    return [y1[0]-y1[-1], y2[0]-y2[-1]]

def fun(y,x, D, N, E, T, w):
    boundary1, boundary2=zero_problem(y, N)
    ODE1, ODE2=compute_yprime(y, x, D, N, E, T, w)
    ODE1=ODE1.reshape((N-1, 1))
    ODE2=ODE2.reshape((N-1, 1))
    c=np.vstack((boundary1, ODE1))
    d=np.vstack((boundary2, ODE2))
    newy=np.vstack((c, d))
    newy=newy.reshape(2*N,)
    return newy
    
def collocation(n):
    # parameters
    E=0.05
    T=0.2
    w=1.2
    D, x=cheb(n)  
    N=np.size(x)
    assert N==11
    assert D.shape==(N, N)
    assert x.shape==(N,)
>>>>>>> HEAD@{9}
    #make initial guess of the point positions, first and last points are same
    #make initial guess of point gradients, first and last gradients are same
<<<<<<< HEAD
    y20=np.zeros(N)
    y0=[0, 1]
    # Integrate the differential equations

    #y1(0)=y1(N)
    #y2(0)=y2(N)
    y0=fsolve(compute_yprime, y0)
    y=odeint(compute_yprime, y0, x, args=(E, T, w))


    pylab.plot(x, y1dot)
    pylab.plot(x, y1)
    pylab.show

=======
    y0=np.zeros((N, 2))
    assert y0.shape==(N, 2)
    y0=y0.flatten(order='C')
    y0=fsolve(fun, y0, args=(x, D, N, E, T, w))
    y1=y0[0:N]
    y1dot=y0[N:2*N]
    pylab.plot(x, y1dot)
    pylab.plot(x, y1)
    pylab.show
    
>>>>>>> HEAD@{9}
collocation(10)
