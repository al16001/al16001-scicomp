import numpy as np
import pylab
from scipy.optimize import fsolve
from cheb import cheb
from week_5_collocation_tests import runtests_ode

  
def ode2(x, t, par):
    """
    ode2(x, t, par)

    Return the right-hand-side of the ODE

        x'' + par[0]*x' + par[1]*x = sin(pi*t)
        
limitation is i had to change the tests for np.sinand np.pi, can i just import numpy without use of prefix?
    """
    return [x[1], np.sin(np.pi*t) - par[0]*x[1] - par[1]*x[0]]

def ode1(x, t, par):
    """
    ode1(x, t, par)

    Return the right-hand-side of the ODE

        x' = sin(pi*t) - x
    """
    return np.sin(np.pi*t) - x
 
def ode3(x, t, pars): 
    """ Return dx/dt = f(x,t) at time t. """ 
    du0=x[1]
    du1=(-2*par[0])*x[1]-x[0]-x[0]**3+par[1]*np.sin(par[2]*t)
    return [du0, du1]
    """
    Compute the area and it's derivative as a function of independent variable x, return them as a vector.
    
    :param x: independent variable, the domain of the problem is x=0 to L
    :return: a 2-vec holding [A, dA/dx].
    """

def compute_x_prime(x, RHS, D, n, dim):
    x_prime=np.zeros((n, dim))
    for j in range(0, dim):
        x_prime[:, j]=np.dot(D[0:n,:],x[:, j])-RHS[0:n, j]
    return x_prime
    
def zero_problem(x, dim):
    boundary=np.zeros(dim)
    for i in range(0, dim):
        boundary[i]=x[0, i]-x[-1, i]
    #first value and last value of every value in x should be the same 
    return boundary

def BVP(x, ode, n, dim):
     D, t=cheb(n)
     RHS=np.zeros((n+1, dim))
     x=x.reshape((n+1, dim), order="F")
     for i in range(0, n):
         RHS[i, :]=ode(x[i, :], t[i], par)
     xpr=compute_x_prime(x, RHS, D, n, dim)
     boundary=zero_problem(x, dim)
     assert xpr.shape==(n, dim)
     BVP=np.vstack((xpr, boundary))
     assert BVP.shape==x.shape
     BVP=BVP.reshape((dim*(n+1),), order="F")
     return BVP




def collocation(ode, n, x0, par):
    D, t=cheb(n)
    N=np.size(t)
    assert D.shape==(N, N)
    assert t.shape==(N,)
    if x0.shape==(N,):
        dim=1
    else:
        (N, dim)=x0.shape
    #this part will iterate and change the input into the ode with each iteration
    #loop required to take input from tests which only returns values for a single point in time seperately.
    RHS=np.zeros((n, dim))
    for i in range(1, n):
        x=np.array(x0)
        x=x.reshape(N, dim)
        assert x.shape==(N, dim)
        RHS[i, :]=ode(x[i, :], t[i], par)
    #make initial guess of the point positions, first and last points are same
    #make initial guess of point gradients, first and last gradients are same
    x0=fsolve(BVP, x0, args=(ode, n, dim))

#    y1=x0[0:N]
#    y1dot=x0[N:2*N]
#    pylab.plot(t, y1dot)
#    pylab.plot(t, y1)
#    pylab.show
#    return x0
par=[0.05, 0.2, 1.2]
n=20
#collocation(ode3, n, np.zeros((n+1, 2)), par)
#runtests_ode(collocation)
collocation(ode3, n, np.zeros((n+1, 2)), par)
